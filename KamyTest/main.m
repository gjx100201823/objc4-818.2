//
//  main.m
//  KamyTest
//
//  Created by jinliang on 2021/4/9.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
        
        // 添加断点，断点到达的时候，可以进入alloc进行调试
        NSObject *obj = [[NSObject alloc] init];
        
        NSLog(@"1233");
        
    }
    return 0;
}
